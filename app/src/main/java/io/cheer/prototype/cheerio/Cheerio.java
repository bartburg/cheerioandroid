package io.cheer.prototype.cheerio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import com.philips.lighting.hue.listener.PHLightListener;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeResource;
import com.philips.lighting.model.PHHueError;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.cheer.prototype.cheerio.interfaces.MicrophoneInputListener;
import io.cheer.prototype.cheerio.util.MicrophoneInput;

import io.cheer.prototype.cheerio.R;

public class Cheerio extends Activity implements MicrophoneInputListener, SharedPreferences.OnSharedPreferenceChangeListener {

    double mOffsetdB = 10;  // Offset for bar, i.e. 0 lit LEDs at 10 dB.
    // The Google ASR input requirements state that audio input sensitivity
    // should be set such that 90 dB SPL at 1000 Hz yields RMS of 2500 for
    // 16-bit samples, i.e. 20 * log_10(2500 / mGain) = 90.
    double mGain = 2500.0 / Math.pow(10.0, 90.0 / 20.0);
    // For displaying error in calibration.
    double mDifferenceFromNominal = 0.0;
    double mRmsSmoothed;  // Temporally filtered version of RMS.
    double mAlpha = 0.4;  // Coefficient of IIR smoothing filter for RMS.
    private int mSampleRate;  // The audio sampling rate to use.
    private int mAudioSource;  // The audio source to use.
    private MicrophoneInput micInput;
    private static final int MAX_HUE=65535;
    private WebView webViewStadium;
    private SharedPreferences sharedPref;
    private int lightbulb =3;
    private int sensitivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        setContentView(R.layout.activity_cheerio);
        micInput = new MicrophoneInput(this);
        webViewStadium = (WebView) findViewById(R.id.webview_stadium);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cheerio, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        readPreferences();

//        getPreferenceScreen().getSharedPreferences()
//                .registerOnSharedPreferenceChangeListener(this);
        micInput.setSampleRate(mSampleRate);
        micInput.setAudioSource(mAudioSource);
        micInput.start();
        webViewStadium.getSettings().setJavaScriptEnabled(true);
        webViewStadium.getSettings().setLoadWithOverviewMode(true);
        webViewStadium.getSettings().setUseWideViewPort(true);
        webViewStadium.loadUrl("http://192.168.1.12:9009/#/stadium");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, PreferencesActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    int methodCounter = 0;
    @Override
    public void processAudioFrame(short[] audioFrame) {
        // Compute the RMS value. (Note that this does not remove DC).
        if(methodCounter > 20)
            methodCounter = 0;
        else if(methodCounter == 20) {
            double rms = 0;
            for (int i = 0; i < audioFrame.length; i++) {
                rms += audioFrame[i] * audioFrame[i];
            }
            rms = Math.sqrt(rms / audioFrame.length);

            // Compute a smoothed version for less flickering of the display.
            mRmsSmoothed = mRmsSmoothed * mAlpha + (1 - mAlpha) * rms;
            final double rmsdB = 20.0 * Math.log10(mGain * mRmsSmoothed);
            DecimalFormat df = new DecimalFormat("##");
            DecimalFormat df_fraction = new DecimalFormat("#");
            final int one_decimal = (int) (Math.round(Math.abs(rmsdB * 10))) % 10;
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    soundPresentation.setText(String.valueOf(rmsdB));
//
//                }
//            });
            Log.d("Volume rmsdb", ""+rmsdB);
            double volume = ((rmsdB-17)/50)*75;
            Log.d("Volume vol", ""+volume);
            int brightnessValue = (int) (volume >= 10 ? 255 / 75 * volume : 0);



            sendHTTPRequest(lightbulb, brightnessValue, 0, brightnessValue, (int) volume);
//            sendHTTPRequest(1, brightnessValue, 32500, brightnessValue, (int) volume);
//            sendHTTPRequest(3, brightnessValue, 0, brightnessValue, (int) volume);
        }
        methodCounter++;
    }

    private void sendHTTPRequest(int lightId, int brightness, int hue, int saturation, int volume) {
        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpGet request = new HttpGet();
            URI website = new URI("http://192.168.1.12:8888/light?light_id="+lightId+"&bri="+brightness+"&hue="+hue+"&sat="+saturation+"&volume="+volume);
            request.setURI(website);
            HttpResponse response = httpclient.execute(request);
//        in = new BufferedReader(new InputStreamReader(
//                response.getEntity().getContent()));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    /**
     * Method to read the sample rate and audio source preferences.
     */
    private void readPreferences() {
        SharedPreferences preferences = getSharedPreferences("LevelMeter",
                MODE_PRIVATE);
        mSampleRate = preferences.getInt("SampleRate", 8000);
        mAudioSource = preferences.getInt("AudioSource",
                MediaRecorder.AudioSource.VOICE_RECOGNITION);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        //sensitivity = sharedPref.getInt("sensitivity", 20);
        //lightbulb = sharedPref.getInt("lightbulb", 1);
    }

    PHLightListener listener = new PHLightListener() {

        @Override
        public void onSuccess() {
        }

        @Override
        public void onStateUpdate(Map<String, String> arg0, List<PHHueError> arg1) {

        }

        @Override
        public void onError(int arg0, String arg1) {}

        @Override
        public void onReceivingLightDetails(PHLight arg0) {}

        @Override
        public void onReceivingLights(List<PHBridgeResource> arg0) {}

        @Override
        public void onSearchComplete() {}
    };

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("sensitivity", sharedPreferences.getInt("sensitivity", 20));
        editor.putInt("lightbulb", sharedPreferences.getInt("lightbulb", 1));
        editor.commit();
    }
}
