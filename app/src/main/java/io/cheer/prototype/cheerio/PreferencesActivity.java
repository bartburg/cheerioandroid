package io.cheer.prototype.cheerio;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Bart on 1-2-2015.
 */
public class PreferencesActivity  extends PreferenceActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SharedPreferences sharedPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        addPreferencesFromResource(R.layout.preferences);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("sensitivity", sharedPreferences.getInt("sensitivity", 20));
        editor.putInt("lightbulb", sharedPreferences.getInt("lightbulb", 3));
        editor.commit();
    }
}
